function onClickButton(id) {
    var request = new XMLHttpRequest();
    console.log('./API-details.php?id=' + id);
    request.open('GET', './API-details.php?id=' + id);
    request.onloadend = function () {
        var res = JSON.parse(request.response);
        console.log(res);
        if (request.status >= 200 && request.status < 400) {
            alert(`La taille moyenne de ce poisson est de ${res[0]['taille']} cm`);
        }
        else {
            console.log('error');
        }
    }
    request.send();
}