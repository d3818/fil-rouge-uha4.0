<?php require_once 'connexion_base.php';
global $bdd;
$reponse = $bdd->query('SELECT * FROM poissons');
$poissons = $reponse->fetchAll();
$reponse = $bdd->query('SELECT * FROM especes');
$especes = $reponse->fetchAll();
$reponse = $bdd->query('SELECT * FROM localisations');
$localisations= $reponse->fetchAll(); ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Classification par espèces</title>
        <link rel="stylesheet" href="ocean.css" />
    </head>
        <body>
            <div>
                <h1>Les espèces</h1>
                <a href="index.php"><h2>Retour au sommaire</h2></a>
                <a href="ocean.php"><h2>Vers l'océan</h2></a>
            </div>
            <div class="conteneur">
                <?php foreach ($especes as $espece){
                    echo "<h2 class='creatureTitle'>" . htmlspecialchars($espece['nom']) . "</h2>" ;
                    echo"<h5 class='creatureTitle'>" . htmlspecialchars($espece['description']) . "</h5>"; ?>
                    <div class="conteneurespece">
                    <?php foreach ($poissons as $poisson) {
                        if ($poisson['type'] == $espece['id']){ ?>
                        <div class="carteEspece">
                            <section>
                                <a href="<?php echo htmlspecialchars($poisson['photo']); ?>" ><img src="<?php echo htmlspecialchars($poisson['photo']); ?>" class="creaturePict" /></a>
                            </section>
                        </div>
                        <?php }
                    }
                 echo '</div>';
                }?>
                    </div>
            </div>
        </body>
</html>
