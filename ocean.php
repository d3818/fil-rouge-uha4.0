<?php require_once 'connexion_base.php';
global $bdd;
$reponse = $bdd->query('SELECT * FROM poissons');
$poissons = $reponse->fetchAll();
$reponse = $bdd->query('SELECT * FROM especes');
$especes = $reponse->fetchAll();
$i = 0; ?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <title>Sous l'océan...</title>
        <link rel="stylesheet" href="ocean.css" />
        <script src="script.js"></script>
    </head>
    <body>
        <div>
            <h1>La faune océanique</h1>
            <a href="index.php"><h2>Retour au sommaire</h2></a>
            <a href="espece.php"><h2>Vers la classification du vivant</h2></a>
        </div>
        <div class="conteneurPoisson">
            <?php foreach ($poissons as $poisson) {
                $reponse = $bdd->query("SELECT localisations.nom FROM poissons
                INNER JOIN fk_poissons_localisations ON fk_poissons_localisations.poissons_id = poissons.id
                INNER JOIN localisations ON localisations.id = fk_poissons_localisations.localisations_id
                WHERE poissons.id = {$poisson['id']}");
                $localisations = $reponse->fetchAll(); ?>
            <div class="carteEspece">
                <section>
                    <img id =""  src="<?php echo htmlspecialchars($poisson['photo']); ?>" class="creaturePict" alt="" />
                    <section class="creatureTitle">
                        <?php echo htmlspecialchars($poisson['nom']); ?>
                    </section>
                </section>
                <section class="carteEspece-Description">
                    <p>Localisation :</p>
                    <ul>
                        <?php foreach ($localisations as $localisation){
                            echo "<li>" . htmlspecialchars($localisation['nom']) . "</li>"; }?>
                    </ul>
                    <div  class="conteneurespece " onclick="(<?php echo htmlspecialchars($poisson['id']); ?>)">
                        <button class="marge2" onclick="onClickButton(<?php echo $poisson['id'];?>)"> VOIR PLUS </button>
                        <div
                            id="<?php echo htmlspecialchars($poisson['id']); ?>" class="containerTab" style="display:none;background:white">
                        </div>
                    </div>
                </section>
            </div>
            <?php $i++; }?>
        </div>
    </body>
</html>