<?php require_once 'connexion_base.php';
$headers = apache_request_headers();
function getDetails($id = "0")
{
    global $bdd;
    $query = "SELECT * FROM details";
    $reponse = array();
    if ($id != "0") {
        $query .= " WHERE id='{$id}' ";
    }
    $result = $bdd->query($query);
    $details = $result->fetchAll(PDO::FETCH_ASSOC);
    foreach ($details as $detail) {
        $reponse[] = $detail;
    }
    header('Content-Type: application/json');
    echo json_encode($reponse, JSON_PRETTY_PRINT);
}
function editDetails($id)
{
    global $sqli_bdd;
    $_PUT = array();
    parse_str(file_get_contents('php://input'), $_PUT);
    $taille = $_PUT["taille"];

    $query="UPDATE details SET taille='".$taille."' WHERE id=".$id;

    if(mysqli_query($sqli_bdd, $query))
    {
        $reponse=array(
            'status' => 1,
            'status_message' =>'Produit mis a jour avec succes.'
        );
    }
    else
    {
    $reponse=array(
        'status' => 0,
        'status_message' =>'Echec de la mise a jour de produit. ');
    }
    header('Content-Type: application/json');
    echo json_encode($reponse, JSON_PRETTY_PRINT);
}
function removeDetails($id)
{
    global $sqli_bdd;
    $reponse = array();
    if (mysqli_query($sqli_bdd, "DELETE FROM details WHERE id={$id}")) {
        $reponse = array('status' => 1, 'status_message' => 'Details retirés');
    } else {
        $reponse = array('status' => 0, 'status_message' => 'Une erreur est survenue lors du retrait des details');
    }
    header('Content-Type: application/json');
    echo json_encode($reponse, JSON_PRETTY_PRINT);
}
function postDetails(){
    global $bdd;
    $valeur = $_POST;
    $req = $bdd->prepare('INSERT INTO details(id, taille) VALUES(:id ,:taille)');
    $req->execute(array(
        'id' => $valeur['id'],
        'taille' => $valeur['taille']
    ));
}
function pagination($page = "0"){
    global $bdd;
    $result = null;
    if ($page > 0){
        $debut = ($page-1) * 5;
        $result = $bdd->query("SELECT * FROM details LIMIT 5 OFFSET $debut");
    }else{
        $result = $bdd->query("SELECT * FROM details");
    }
    $Details = $result->fetchAll(PDO:: FETCH_ASSOC);
    if(!$Details){
        $info = 'Aucun detail sur cette page';
        header('Content-Type: application/json');
        echo json_encode($info , JSON_PRETTY_PRINT);
    }
    else {
        header('Content-Type: application/json');
        echo json_encode($Details, JSON_PRETTY_PRINT);
    }
}
$request_method = $_SERVER["REQUEST_METHOD"];
switch ($request_method) {
    case 'GET':
        if (!empty($_GET["id"])) {
            getdetails($_GET["id"]);
        }
        elseif(!empty($_GET['page'])){
            $page = intval($_GET['page']);
            pagination($page);
        }
         else {
            getdetails();
        }
        break;
    case 'POST':
        postDetails();
        break;
    case 'PUT':
        editDetails($_GET['id']);
        break;
    case 'DELETE':
        removeDetails($_GET['id']);
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        break;
} ?>