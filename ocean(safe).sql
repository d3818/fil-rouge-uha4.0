DROP DATABASE IF EXISTS ocean;
CREATE DATABASE ocean;
USE ocean;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `details` (
    `id` int(255) NOT NULL,
    `taille` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `especes` (
    `id` int(255) NOT NULL,
    `nom` varchar(255) DEFAULT NULL,
    `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `fk_poissons_localisations` (
    `poissons_id` int(11) NOT NULL,
    `localisations_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `localisations` (
    `id` int(11) NOT NULL,
    `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `poissons` (
    `id` int(255) NOT NULL,
    `nom` varchar(255) DEFAULT NULL,
    `type` int(255) NOT NULL,
    `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

ALTER TABLE `details`
    ADD PRIMARY KEY (`id`,`taille`),
  ADD KEY `id` (`id`);

ALTER TABLE `especes`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `fk_poissons_localisations`
    ADD PRIMARY KEY (`poissons_id`,`localisations_id`),
  ADD KEY `fk_localisations` (`localisations_id`);

ALTER TABLE `localisations`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `poissons`
    ADD PRIMARY KEY (`id`),
  ADD KEY `poissons_ibfk_1` (`type`);

ALTER TABLE `details`
    MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

ALTER TABLE `especes`
    MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `localisations`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `poissons`
    MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `details`
    ADD CONSTRAINT `details_ibfk_1` FOREIGN KEY (`id`) REFERENCES `poissons` (`id`);

ALTER TABLE `fk_poissons_localisations`
    ADD CONSTRAINT `fk_localisations` FOREIGN KEY (`localisations_id`) REFERENCES `localisations` (`id`),
  ADD CONSTRAINT `fk_poissons` FOREIGN KEY (`poissons_id`) REFERENCES `poissons` (`id`);

ALTER TABLE `poissons`
    ADD CONSTRAINT `poissons_ibfk_1` FOREIGN KEY (`type`) REFERENCES `especes` (`id`);
COMMIT;
