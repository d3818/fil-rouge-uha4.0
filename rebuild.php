<?php include 'connexion_base.php';
global $bdd;

$donnee_poisson =file_get_contents('https://filrouge.uha4point0.fr/ocean/poissons');
$donnee_poisson =json_decode($donnee_poisson,true);
$donnee_espece = file_get_contents('https://filrouge.uha4point0.fr/ocean/especes');
$donnee_espece = json_decode($donnee_espece, true);
$sql = file_get_contents('ocean(safe).sql');
$bdd->query($sql);

foreach($donnee_espece as $espece) {
    $req = $bdd->prepare('INSERT INTO especes(nom, description) VALUES(:nom, :description)');
    $req->execute(array(
        'nom' => $espece['nom'],
        'description' => $espece['description']
    )) or die(print_r($req->errorInfo()));
}
foreach($donnee_poisson as $poissons) {
    global $sqli_bdd;
    $req = $bdd->prepare('INSERT INTO poissons(nom, type, photo) VALUES(:nom, :genre, :photo)');
    $req->execute(array(
        'nom' => $poissons['nom'],
        'genre' => $poissons['type'],
        'photo' => $poissons['photo']
    )) or die(print_r($req->errorInfo()));

    foreach($poissons["localisations"] as $localisations ){
        $ponctuation = mysqli_real_escape_string($sqli_bdd,$localisations);
        $req = $bdd->prepare('INSERT INTO localisations(nom) VALUES(:nom)');
        $req->execute(array(
            'nom' => $localisations
        )) or die(print_r($req->errorInfo()));
        $loc = $bdd->query("SELECT id FROM localisations WHERE nom='{$ponctuation}'")->fetchAll()[0]['id'];
        $poc = $bdd->query("SELECT id FROM poissons WHERE nom='{$poissons['nom']}'")->fetchAll()[0]['id'];
        $req = $bdd->prepare('INSERT INTO fk_poissons_localisations(poissons_id, localisations_id) VALUES(:pid, :lid)');
        $req->execute(array(
            'pid' => $poc,
            'lid' => $loc
        )) or die(print_r($req->errorInfo()));
    }
}
?>